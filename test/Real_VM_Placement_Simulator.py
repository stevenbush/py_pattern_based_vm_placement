__author__ = 'jiyuanshi'
"""
This script is used to simulate the real VM placement at a cloud data center (consider the VM with multiple VM types)
"""

import random
import simpy
import ConfigParser, sys, glob, math

FLAG = False
SIM_TIME = 500  # Simulation time in minutes
RANDOM_SEED = 42


def neg_exp(lambd):
    '''
    a simple function to sample from negative exponential
    :param lambd:
    :return:
    '''
    return random.expovariate(lambd)


class VM(object):
    '''
    this a VM class
    '''

    def __init__(self, type, id, run_time, arrival_time, demands_list):
        self.type = type
        self.id = id
        self.run_time = run_time
        self.arrval_time = arrival_time
        self.start_time = 0
        self.end_time = 0
        self.wait_time = 0
        self.demands_list = []
        for i in range(len(demands_list)):
            self.demands_list.append(demands_list[i])


class Server(object):
    '''
    this is a server class
    '''

    def __init__(self, id, pattern, capacity_list, ready_server_list):
        self.id = id
        self.isbusy = False

        self.pattern = []
        for i in range(len(pattern)):
            self.pattern.append(pattern[i])

        self.workload = []
        for i in range(len(pattern)):
            self.workload.append(0)

        self.capacity_list = []
        for i in range(len(capacity_list)):
            self.capacity_list.append(capacity_list[i])

        self.ready_server_list = ready_server_list
        for i in range(len(pattern)):
            if pattern[i] > 0:
                self.ready_server_list[i].append(self)

    def acquire_resource(self, vm):
        """
        acquire the resource
        :param vm:
        :return:
        """
        self.workload[vm.type] = self.workload[vm.type] + 1

        if (self.workload[vm.type] == self.pattern[vm.type]) and self.workload[vm.type] > 0:
            self.ready_server_list[vm.type].remove(self)

    def release_resource(self, vm):
        """
        release the resource
        :param vm:
        :return:
        """

        self.workload[vm.type] = self.workload[vm.type] - 1

        if (self.pattern[vm.type] - self.workload[vm.type] == 1) and self.pattern[vm.type] > 0:
            self.ready_server_list[vm.type].append(self)

    def checking(self, vm):
        """
        checking if this vm can be accomodation in this server based on pattern information
        :param vm:
        :return:
        """
        flag = True

        if self.pattern[vm.type] - self.workload[vm.type] == 0:
            flag = False

        return flag


class DataCenter(object):
    '''
    this is a data center class
    '''

    def __init__(self, env, pattern_num_list, patterns_list, capacity_list, ready_server_list):
        self.env = env
        self.vms_list = []
        self.ready_server_list = ready_server_list
        # set up servers
        self.servers_list = []
        for i in range(len(patterns_list)):
            current_server_num = pattern_num_list[i]
            for j in range(current_server_num):
                self.servers_list.append(Server(i + j, patterns_list[i], capacity_list, ready_server_list))

    def add_vm(self, vm):
        '''
        adding a vm into the waiting queue of this data center
        :param vm:
        :return:
        '''
        self.vms_list.append(vm)
        self.schedule()

    def run_vm(self, vm, server):
        """
        run a vm on a server
        :param vm:
        :param server:
        :return:
        """
        if FLAG:
            print('vm%s-%s enters the server%s at %.2f.' % (vm.type, vm.id, server.id, self.env.now))
        vm.start_time = self.env.now
        yield self.env.timeout(vm.run_time)
        if FLAG:
            print("vm%s-%s complete its %s minutes execution." % (vm.type, vm.id, vm.run_time))
            print('vm%s-%s leaves the server%s at %.2f.' % (vm.type, vm.id, server.id, self.env.now))
        vm.end_time = self.env.now
        vm.wait_time = vm.start_time - vm.arrval_time
        server.release_resource(vm)
        self.schedule()

    def schedule(self):
        """
        scheduling VMs based on pattern
        :return:
        """
        scheduled_vms = []
        for i in range(len(self.vms_list)):
            current_vm = self.vms_list[i]
            vm_type = current_vm.type
            current_server_list = self.ready_server_list[vm_type]
            for j in range(len(current_server_list)):
                current_server = current_server_list[j]
                if current_server.checking(current_vm):
                    current_server.acquire_resource(current_vm)
                    self.env.process(self.run_vm(current_vm, current_server))  # scheduling these vms for execution
                    scheduled_vms.append(current_vm)
                    break
                else:
                    continue

        # remove these scheduled vms
        for i in range(len(scheduled_vms)):
            self.vms_list.remove(scheduled_vms[i])


def Generator(env, type, datacenter, lamda, mu, vm_demands, vm_depository):
    # Create vms while the simulation is running
    i = 0
    while True:
        yield env.timeout(neg_exp(lamda))
        i += 1
        vm = VM(type, i, neg_exp(mu), env.now, vm_demands)
        if FLAG:
            print('vm%s-%s arrives at %.2f.' % (vm.type, vm.id, env.now))
        datacenter.add_vm(vm)
        vm_depository.append(vm)


def cal_cos_val(vm_demands, current_used_resource):
    '''
    calculating the cos value when placing this VM into this server
    :param vm_demands:
    :param current_used_resource:
    :return:
    '''
    result1 = 0
    result2 = 0
    result3 = 0
    for i in range(len(vm_demands)):
        result1 = result1 + vm_demands[i] * current_used_resource[i]
        result2 = result2 + vm_demands[i] * vm_demands[i]
        result3 = result3 + current_used_resource[i] * current_used_resource[i]

    cos_value = 0
    if result3 > 0:
        cos_value = result1 / (math.sqrt(result2 * result3))
    return cos_value


def cal_current_used_resource(vm_type, dimension_num, vm_demands_list, server_pattern):
    """
    calculating the resource usage of a server pattern
    :param vm_type:
    :param dimension_num:
    :param vm_demands_list:
    :param server_pattern:
    :return:
    """
    used_resource_list = []
    for i in range(dimension_num):
        used_resource = 0
        for j in range(vm_type):
            if server_pattern[j] > 0:
                used_resource = used_resource + vm_demands_list[j][i] * server_pattern[j]
        used_resource_list.append(used_resource)
    return used_resource_list


def cal_Max_DVol_value(capacity, current_used_resource):
    '''
    calculating the Max_DVol_value of this server pattern
    :param capacity:
    :param current_used_resource:
    :return:
    '''
    dvol_val = 0
    for i in range(len(current_used_resource)):
        dvol_val = dvol_val * ((capacity * 1.0) / (capacity - current_used_resource[i]))
    return dvol_val


def cal_vectordot(vm_demands, current_used_resource):
    '''
    calculating the vectordot value when placing this VM into this server
    :param vm_demands:
    :param current_used_resource:
    :return:
    '''
    result = 0
    for i in range(len(vm_demands)):
        result = result + vm_demands[i] * current_used_resource[i]
    return result


def cal_after_used_resource(vm_demands, current_used_resource):
    '''
    calculating the resource usage of a server pattern after placing this VM
    :param current_used_resource:
    :return:
    '''
    after_used_resource = []
    for i in range(len(vm_demands)):
        current_val = vm_demands[i] + current_used_resource[i]
        after_used_resource.append(current_val)

    return after_used_resource


def firstfit_checking(vm_demands, server_used_resource, capacity):
    """
    checking server pattern based in firstfit policy
    :param vm_demands:
    :param server_used_resource:
    :param capacity:
    :return:
    """
    flag = True
    for i in range(len(vm_demands)):
        total_used_resource = vm_demands[i] + server_used_resource[i]
        if total_used_resource > capacity:
            flag = False
            break
    return flag


def firstfit_generation(raw_cf, pattern_cf):
    """
    generating server configuration pattern based on firstfit policy
    :param raw_cf:
    :param pattern_cf:
    :return:
    """
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing firstfit pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                server_warehouse[i][current_vm_type] = server_warehouse[i][current_vm_type] + 1
                break
        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def max_min_generation(raw_cf, pattern_cf):
    """
    generating server configuration pattern based on maxmin policy
    :param raw_cf:
    :param pattern_cf:
    :return:
    """
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing bestfit max min pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        current_vm__demands = item_type_list[current_vm_type]
        maxmin_index = 0
        maxmin_val = 0
        find_flag = False
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                current_maxmin_val = max(cal_after_used_resource(current_vm__demands, used_resource_list))
                if current_maxmin_val >= maxmin_val:
                    maxmin_val = current_maxmin_val
                    maxmin_index = i
                    find_flag = True

        if find_flag:
            server_warehouse[maxmin_index][current_vm_type] = server_warehouse[maxmin_index][current_vm_type] + 1

        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def vectordot_generation(raw_cf, pattern_cf):
    """
    generating server configuration pattern based on vectordot policy
    :param raw_cf:
    :param pattern_cf:
    :return:
    """
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing bestfit max min pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        current_vm__demands = item_type_list[current_vm_type]
        vectordot_index = 0
        vectordo_val = sys.maxint
        find_flag = False
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                current_vectordo_val = cal_vectordot(current_vm__demands, used_resource_list)
                if current_vectordo_val < vectordo_val:
                    vectordo_val = current_vectordo_val
                    vectordot_index = i
                    find_flag = True

        if find_flag:
            server_warehouse[vectordot_index][current_vm_type] = server_warehouse[vectordot_index][current_vm_type] + 1

        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def cos_generation(raw_cf, pattern_cf):
    '''
    generating server configuration pattern based on min cos value policy
    :param raw_cf:
    :param pattern_cf:
    :return:
    '''
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing bestfit max min pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        current_vm__demands = item_type_list[current_vm_type]
        cos_index = 0
        cos_val = sys.maxint
        find_flag = False
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                current_cos_val = cal_cos_val(current_vm__demands, used_resource_list)
                if current_cos_val < cos_val:
                    cos_val = current_cos_val
                    cos_index = i
                    find_flag = True

        if find_flag:
            server_warehouse[cos_index][current_vm_type] = server_warehouse[cos_index][current_vm_type] + 1

        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def Max_DVol_generation(raw_cf, pattern_cf):
    '''
    generating server configuration pattern based on min Max_DVol value policy
    :param raw_cf:
    :param pattern_cf:
    :return:
    '''
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing bestfit max min pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        current_vm__demands = item_type_list[current_vm_type]
        DVol_index = 0
        DVol_val = sys.maxint
        find_flag = False
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                current_DVol_val = cal_Max_DVol_value(capacity, used_resource_list)
                if current_DVol_val < DVol_val:
                    DVol_val = current_DVol_val
                    DVol_index = i
                    find_flag = True

        if find_flag:
            server_warehouse[DVol_index][current_vm_type] = server_warehouse[DVol_index][current_vm_type] + 1

        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def pattern_generation(cf):
    """
    generating server configuration pattern based on cutting stock model
    :param cf:
    :return:
    """
    patterns_list = []
    pattern_num_list = []
    pattern_types = cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_str = cf.get('pattern_types', str(i))
        pattern_str_list = pattern_str.split(',')
        pattern = []
        for j in range(len(pattern_str_list)):
            pattern.append(int(pattern_str_list[j].strip()))
        patterns_list.append(pattern)

        pattern_num = cf.getint('pattern_num', str(i))
        pattern_num_list.append(pattern_num)

    return patterns_list, pattern_num_list


def Simulation(input_path, raw_file_name, pattern_file_name, schedule_method):
    # reading basic configuration
    raw_cf = ConfigParser.ConfigParser()
    conf_file_path = input_path + '/' + raw_file_name
    raw_cf.read(conf_file_path)
    # reading server pattern configuration
    pattern_cf = ConfigParser.ConfigParser()
    conf_file_path = input_path + '/' + pattern_file_name
    pattern_cf.read(conf_file_path)

    capacity = raw_cf.getint('baseconf', 'server_capacity')
    if FLAG:
        print('capacity: ' + str(capacity))
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    if FLAG:
        print('type_num: ' + str(type_num))
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')
    if FLAG:
        print('dimension_num: ' + str(dimension_num))

    # setting the server list which is used for store servers can accommodate corresponding type of VMs
    ready_server_list = []
    for i in range(type_num):
        new_list = []
        ready_server_list.append(new_list)

    # setting server configuration
    server_conf = []
    for i in range(dimension_num):
        server_conf.append(capacity)
    if FLAG:
        print('server_conf: ' + str(server_conf))

    vm_demand_list = []  # reading the VM demands
    vm_lambd_list = []  # reading the arrival rate of each vm type
    vm_mu_list = []  # reading the serivice rate of each vm type
    vm_delay_list = []  # reading the delay threshold of each vm type
    for i in range(type_num):
        demand_str = raw_cf.get('VM_demands', str(i))
        demand_list = demand_str.split(',')
        vm_demand = []
        for j in range(len(demand_list)):
            vm_demand.append(int(demand_list[j].strip()))
        vm_demand_list.append(vm_demand)
        vm_lambd = raw_cf.getint('arrival_rate', str(i))
        vm_lambd_list.append(vm_lambd)
        vm_mu = raw_cf.getfloat('service_rate', str(i))
        vm_mu_list.append(vm_mu)
        vm_dealy = raw_cf.getint('delay', str(i))
        vm_delay_list.append(vm_dealy)
    if FLAG:
        print('vm_demand_list: ' + str(vm_demand_list))
        print('vm_lambd_list: ' + str(vm_lambd_list))
        print('vm_mu_list: ' + str(vm_mu_list))
        print('vm_delay_list: ' + str(vm_delay_list))

    # setting VM depository list
    vm_depository_list = []
    for i in range(type_num):
        vm_depository = []
        vm_depository_list.append(vm_depository)

    if schedule_method == 'pattern':
        patterns_list, pattern_num_list = pattern_generation(pattern_cf)
    if schedule_method == 'firstfit':
        patterns_list, pattern_num_list = firstfit_generation(raw_cf, pattern_cf)
    if schedule_method == 'maxmin':
        patterns_list, pattern_num_list = max_min_generation(raw_cf, pattern_cf)
    if schedule_method == 'vectordot':
        patterns_list, pattern_num_list = vectordot_generation(raw_cf, pattern_cf)
    if schedule_method == 'cos':
        patterns_list, pattern_num_list = cos_generation(raw_cf, pattern_cf)
    if schedule_method == 'maxdvol':
        patterns_list, pattern_num_list = Max_DVol_generation(raw_cf, pattern_cf)

    if FLAG:
        print('patterns_list: ' + str(patterns_list))
        print('pattern_num_list: ' + str(pattern_num_list))

    print('start simulating...')

    # Create an environment and start the setup process
    env = simpy.Environment()

    datacenter = DataCenter(env, pattern_num_list, patterns_list, server_conf, ready_server_list)

    for i in range(type_num):
        env.process(
            Generator(env, i, datacenter, vm_lambd_list[i], vm_mu_list[i], vm_demand_list[i], vm_depository_list[i]))

    # Execute!
    env.run(until=SIM_TIME)

    print('end simulating...')

    # Find finished vm
    total_finished_num = 0
    finished_vm_list = []
    for i in range(type_num):
        finished_vms = []
        total_finished_num = total_finished_num + len(vm_depository_list[i])
        for vm in vm_depository_list[i]:
            if vm.end_time > 0:
                finished_vms.append(vm)
        finished_vm_list.append(finished_vms)

    exceed_vm_num = 0
    for i in range(type_num):
        for vm in finished_vm_list[i]:
            if vm.wait_time > vm_delay_list[i]:
                exceed_vm_num = exceed_vm_num + 1

    exceed_proportion = (exceed_vm_num * 1.0) / total_finished_num

    # output summary statistics to screen
    # print ""
    # print "Summary results:"
    # print "Exceed Proportion: ", exceed_proportion
    # print ""

    return exceed_proportion


if __name__ == '__main__':

    random.seed(RANDOM_SEED)

    if len(sys.argv) < 6:
        print('No input_path schedule_method simulation_num wildcard sim_time specified!!!!')
        print('Please enter python script_name input_path schedule_method simulation_num wildcard sim_time')
        sys.exit()

    schedule_method = str(sys.argv[2])
    simulation_num = int(sys.argv[3])
    wildcard = str(sys.argv[4])
    SIM_TIME = int(sys.argv[5])

    if schedule_method != 'pattern' and schedule_method != 'firstfit' and schedule_method != 'maxmin' and schedule_method != 'vectordot' and schedule_method != 'cos' and schedule_method != 'maxdvol':
        print('wrong scheduling method input!!!')
        sys.exit()

    random_seed_list = []
    for i in range(simulation_num):
        random_seed_list.append(random.randint(10, 60))

    # getting configuration files
    input_path = str(sys.argv[1])
    overall_ave_result_file_name = 'overall_ave_result_' + schedule_method + '.ini'
    overall_ave_f = open(input_path + '/' + overall_ave_result_file_name, 'w')
    file_list = glob.glob(input_path + '/' + 'raw*' + wildcard + '.ini')
    for file in file_list:
        tmp_str = file.split('/')
        raw_file_name = tmp_str[len(tmp_str) - 1]
        # print('raw_file_namen is: %s' % (raw_file_name))
        pattern_file_name = 'pattern_' + raw_file_name
        # print('pattern_file_name is: %s' % (pattern_file_name))
        result_file_name = 'result_' + schedule_method + '_' + raw_file_name
        ave_result_file_name = 'ave_result_' + schedule_method + '_' + raw_file_name
        f = open(input_path + '/' + result_file_name, 'w')
        ave_f = open(input_path + '/' + ave_result_file_name, 'w')
        sum_exceed_proportion = 0
        ave_counter = 0

        for i in range(simulation_num):
            random.seed(random_seed_list[i])
            print('start simulating %s for %s' % (raw_file_name, i))
            exceed_proportion = Simulation(input_path, raw_file_name, pattern_file_name, schedule_method)
            f.write(str(exceed_proportion) + '\n')
            ave_counter = ave_counter + 1
            sum_exceed_proportion = sum_exceed_proportion + exceed_proportion

        ave_exceed_proportion = (sum_exceed_proportion * 1.0) / ave_counter
        ave_f.write(str(ave_exceed_proportion) + '\n')
        overall_ave_f.write(str(ave_exceed_proportion) + '\n')
        f.close()
        ave_f.close()
        overall_ave_f.flush()

    overall_ave_f.close()
