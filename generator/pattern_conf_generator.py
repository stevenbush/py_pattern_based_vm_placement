__author__ = 'jiyuanshi'
"""
This script is used to generate the pattern configuration files
"""

import ConfigParser, string, os, sys, random, glob
import vector_cutstock_solver


def ReadingConfFile(conf_file_path):
    cf = ConfigParser.ConfigParser()
    cf.read(conf_file_path)

    capacity = cf.getint('baseconf', 'server_capacity')
    type_num = cf.getint('baseconf', 'num_vm_types')
    dimension_num = cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    return item_type_list, type_ave_num_list, capacity_list


def Generating_Pattern(input_path, input_file_name):
    out_file_name = 'pattern_' + input_file_name
    s, q, B = ReadingConfFile(input_path + '/' + input_file_name)
    patterns_list, rolls = vector_cutstock_solver.solveCuttingStock(s, q, B)
    # print len(rolls), "rolls:"
    # print('paterns_num: ' + str(len(patterns_list)))
    # print('patterns_list: ')
    sorted_list = sorted(patterns_list.items(), lambda x, y: cmp(x[1], y[1]), reverse=True)
    # for item in sorted_list:
    #     print(str(item[0]) + ':' + str(item[1]))

    cfgfile = open(input_path + '/' + out_file_name, 'w')
    cf = ConfigParser.ConfigParser()

    # adding the 'pattern' section
    cf.add_section('pattern_types')
    for i in range(len(sorted_list)):
        pattern_type = sorted_list[i][0][1:-1]
        cf.set('pattern_types', str(i), pattern_type)

    # adding the 'pattern_num' section
    cf.add_section('pattern_num')
    for i in range(len(sorted_list)):
        pattern_num = sorted_list[i][1]
        cf.set('pattern_num', str(i), pattern_num)

    cf.write(cfgfile)
    cfgfile.close()


if __name__ == '__main__':

    if len(sys.argv) < 2:
        print('No input_path specified!!!!')
        print('Please enter python script_name input_path')
        sys.exit()

    input_path = str(sys.argv[1])
    file_list = glob.glob(input_path + '/' + 'raw*.ini')
    for file in file_list:
        tmp_str = file.split('/')
        file_name = tmp_str[len(tmp_str) - 1]
        print('processing file: %s' % (file_name))
        Generating_Pattern(input_path, file_name)
