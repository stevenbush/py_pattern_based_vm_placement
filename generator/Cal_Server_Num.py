__author__ = 'jiyuanshi'
'''
This script is used to calculated some parameters of a M/M/c queuing system based on queueing theory
'''

import math


def cal_pi_zero(c, lamada, mu):
    '''
    Calculating the probability of system have 0 customers

    c: the number of server in this system
    '''
    p = lamada * 1.0 / (c * mu)
    component3 = 0
    for k in range(c):
        component3 = component3 + (1.0 / math.factorial(k)) * math.pow(p * c, k)
    component4 = (1.0 / math.factorial(c)) * math.pow(p * c, c) * (1 / (1 - p))
    my_pi = math.pow((component3 + component4), -1)
    return my_pi


def cal_pi_small(n, c, lamada, mu):
    '''
    Calculating the probability of system have n customers, where n <= c

    c: the number of server in this system
    '''
    p = lamada * 1.0 / (c * mu)
    component1 = 1.0 / math.factorial(n)
    component2 = math.pow(p * c, n)
    my_pi = component1 * component2 * cal_pi_zero(c, lamada, mu)
    return my_pi


def cal_pi_large(n, c, lamada, mu):
    '''
    Calculating the probability of system have n customers, where n > c

    c: the number of server in this system
    '''
    p = lamada * 1.0 / (c * mu)
    component1 = 1.0 / math.factorial(c)
    component2 = 1 / (math.pow(c, n - c))
    component3 = math.pow(p * c, n)
    my_pi = component1 * component2 * component3 * cal_pi_zero(c, lamada, mu)
    return my_pi


def cal_average_wait(c, lamada, mu):
    '''
    Calculating the average wait time of this system with c servers

    c: the number of server in this system
    '''
    p = lamada * 1.0 / (c * mu)
    component1 = cal_pi_small(c, c, lamada, mu)
    component2 = c * mu * math.pow(1 - p, 2)
    my_average = component1 / component2
    return my_average


def cal_portion(wait_time, c, lamada, mu):
    '''
    Calculating the proportion of the customer's waiting time is larger than the given wait_time
    :param wait_time: the given wait_time
    :param c: the number of servers in this system
    :param lamada: the arrival rate
    :param mu: the service rate
    :return:
    '''
    p = lamada * 1.0 / (c * mu)
    pi_c = cal_pi_small(c, c, lamada, mu)
    portion = (pi_c * 1.0 / (1 - p)) * math.pow(math.e, -1 * c * mu * wait_time * (1 - p))
    return portion


def CalServerNum(wait_thre, portion_thre, lamada, mu):
    """
    Calculating the minimum number of needed servers that can meet the wait threshold and the exceeding proportion
    :param wait_thre:
    :param portion_thre:
    :param lamada:
    :param mu:
    :return:
    """
    current_num = int(math.ceil((lamada / mu) + 1))
    portion = cal_portion(wait_thre, current_num, lamada, mu)
    while portion > portion_thre:
        current_num = current_num + 1
        portion = cal_portion(wait_thre, current_num, lamada, mu)
    return current_num


if __name__ == '__main__':
    print('cal_average_wait: ' + str(cal_average_wait(49, 2, 1.0 / 20)))
    print('cal_portion: ' + str(cal_portion(0.5, 49, 2, 1.0 / 20)))
    print('CalServerNum: ' + str(CalServerNum(2, 0.05, 2, 1.0 / 20)))
