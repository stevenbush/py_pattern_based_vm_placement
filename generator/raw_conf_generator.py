__author__ = 'jiyuanshi'
"""
This script is used to generate the raw configuration files, which contains input parameters
"""

import ConfigParser, string, os, sys, random, math
import Cal_Server_Num

NUM_OF_DIMENSIONS = 6
SERVER_CAPACITY = 100
NUM_VM_TYPES = 6
ARRIVAL_RATE_MIN = 2
ARRIVAL_RATE_MAX = 6
SERVICE_TIME_MIN = 15
SERVICE_TIME_MAX = 20
VM_DEMANDS_MIN = 5
VM_DEMANDS_MAX = 35
DELAY_MIN = 1
DELAY_MAX = 10
EXCEED_PORTION = 0.05
GENERATE_FILE_NUM = 10
RANDOM_SEED = 15


def generate_conf_files(out_path, file_name, relation_type):
    cfgfile = open(out_path + "/" + file_name + ".ini", 'w')
    cf = ConfigParser.ConfigParser()

    # adding the basic configuration section
    cf.add_section('baseconf')
    # adding the specific items of this sectiojn
    cf.set('baseconf', 'num_of_dimensions', NUM_OF_DIMENSIONS)
    cf.set('baseconf', 'server_capacity', SERVER_CAPACITY)
    cf.set('baseconf', 'num_VM_types', NUM_VM_TYPES)
    cf.set('baseconf', 'exceed_portion', EXCEED_PORTION)

    # adding the 'VM_demands' section

    command = 'randdataset -%s -d %s -n %s -s %s' % (
        relation_type, NUM_OF_DIMENSIONS, NUM_VM_TYPES, random.randint(1, 1000))
    r = os.popen(command)
    result = r.readlines()
    cf.add_section('VM_demands')
    for i in range(NUM_VM_TYPES):
        line = result[i]
        line = line.strip('\r\n')
        str_list = line.split(',')
        current_demands = [0] * len(str_list)
        for j in range(len(str_list)):
            val = VM_DEMANDS_MIN + float(str_list[j]) * (VM_DEMANDS_MAX - VM_DEMANDS_MIN)
            current_demands[j] = int(math.ceil(val))
        demands_str = str(current_demands[0])
        for j in range(len(current_demands) - 1):
            demands_str = demands_str + ',' + str(current_demands[j + 1])
        cf.set('VM_demands', str(i), demands_str)

    # adding the 'delay' section
    delay_list = []
    cf.add_section('delay')
    for i in range(NUM_VM_TYPES):
        current_delay = random.randint(DELAY_MIN, DELAY_MAX)
        cf.set('delay', str(i), current_delay)
        delay_list.append(current_delay)

    # adding the 'arrival rate' section
    lamada_list = []
    cf.add_section('arrival_rate')
    for i in range(NUM_VM_TYPES):
        current_rate = random.randint(ARRIVAL_RATE_MIN, ARRIVAL_RATE_MAX)
        cf.set('arrival_rate', str(i), current_rate)
        lamada_list.append(current_rate)

    # adding the 'service rate' section
    mu_list = []
    cf.add_section('service_rate')
    for i in range(NUM_VM_TYPES):
        current_rate = 1.0 / random.randint(SERVICE_TIME_MIN, SERVICE_TIME_MAX)
        cf.set('service_rate', str(i), current_rate)
        mu_list.append(current_rate)

    # adding the 'num_of_service_block' section
    cf.add_section('num_of_block')
    for i in range(NUM_VM_TYPES):
        current_num_of_block = Cal_Server_Num.CalServerNum(delay_list[i], EXCEED_PORTION, lamada_list[i], mu_list[i])
        cf.set('num_of_block', str(i), current_num_of_block)

    cf.write(cfgfile)
    cfgfile.close()


if __name__ == '__main__':
    if len(sys.argv) < 6:
        print('No out_path file_numbers num_VM_types num_dimensions relation_type specified!!!!')
        print('Please enter python script_name out_path file_numbers num_VM_types num_dimensions relation_type')
        sys.exit()

    out_path = str(sys.argv[1])
    GENERATE_FILE_NUM = int(sys.argv[2])
    NUM_VM_TYPES = int(sys.argv[3])
    NUM_OF_DIMENSIONS = int(sys.argv[4])
    relation_type = str(sys.argv[5])

    for i in range(GENERATE_FILE_NUM):
        file_name = 'raw_' + str(i)
        generate_conf_files(out_path, file_name, relation_type)
