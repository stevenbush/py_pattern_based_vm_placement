import random
import ConfigParser, sys, math


def cal_Max_DVol_value(capacity, current_used_resource):
    dvol_val = 0
    for i in range(len(current_used_resource)):
        dvol_val = dvol_val * ((capacity * 1.0) / (capacity - current_used_resource[i]))
    return dvol_val


def cal_cos_val(vm_demands, current_used_resource):
    result1 = 0
    result2 = 0
    result3 = 0
    for i in range(len(vm_demands)):
        result1 = result1 + vm_demands[i] * current_used_resource[i]
        result2 = result2 + vm_demands[i] * vm_demands[i]
        result3 = result3 + current_used_resource[i] * current_used_resource[i]

    cos_value = 0
    if result3 > 0:
        cos_value = result1 / (math.sqrt(result2 * result3))
    return cos_value


def cal_vectordot(vm_demands, current_used_resource):
    result = 0
    for i in range(len(vm_demands)):
        result = result + vm_demands[i] * current_used_resource[i]
    return result


def cal_current_used_resource(vm_type, dimension_num, vm_demands_list, server_pattern):
    used_resource_list = []
    for i in range(dimension_num):
        used_resource = 0
        for j in range(vm_type):
            if server_pattern[j] > 0:
                used_resource = used_resource + vm_demands_list[j][i] * server_pattern[j]
        used_resource_list.append(used_resource)
    return used_resource_list


def cal_after_used_resource(vm_demands, current_used_resource):
    after_used_resource = []
    for i in range(len(vm_demands)):
        current_val = vm_demands[i] + current_used_resource[i]
        after_used_resource.append(current_val)

    return after_used_resource


def firstfit_checking(vm_demands, server_used_resource, capacity):
    flag = True
    for i in range(len(vm_demands)):
        total_used_resource = vm_demands[i] + server_used_resource[i]
        if total_used_resource > capacity:
            flag = False
            break
    return flag


def firstfit_generation(raw_cf, pattern_cf):
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing firstfit pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                server_warehouse[i][current_vm_type] = server_warehouse[i][current_vm_type] + 1
                break
        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def max_min_generation(raw_cf, pattern_cf):
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing bestfit max min pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        current_vm__demands = item_type_list[current_vm_type]
        maxmin_index = 0
        maxmin_val = 0
        find_flag = False
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                current_maxmin_val = max(cal_after_used_resource(current_vm__demands, used_resource_list))
                if current_maxmin_val >= maxmin_val:
                    maxmin_val = current_maxmin_val
                    maxmin_index = i
                    find_flag = True

        if find_flag:
            server_warehouse[maxmin_index][current_vm_type] = server_warehouse[maxmin_index][current_vm_type] + 1

        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def vectordot_generation(raw_cf, pattern_cf):
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing bestfit max min pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        current_vm__demands = item_type_list[current_vm_type]
        vectordot_index = 0
        vectordo_val = sys.maxint
        find_flag = False
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                current_vectordo_val = cal_vectordot(current_vm__demands, used_resource_list)
                if current_vectordo_val < vectordo_val:
                    vectordo_val = current_vectordo_val
                    vectordot_index = i
                    find_flag = True

        if find_flag:
            server_warehouse[vectordot_index][current_vm_type] = server_warehouse[vectordot_index][current_vm_type] + 1

        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def cos_generation(raw_cf, pattern_cf):
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing bestfit max min pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        current_vm__demands = item_type_list[current_vm_type]
        cos_index = 0
        cos_val = sys.maxint
        find_flag = False
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                current_cos_val = cal_cos_val(current_vm__demands, used_resource_list)
                if current_cos_val < cos_val:
                    cos_val = current_cos_val
                    cos_index = i
                    find_flag = True

        if find_flag:
            server_warehouse[cos_index][current_vm_type] = server_warehouse[cos_index][current_vm_type] + 1

        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


def Max_DVol_generation(raw_cf, pattern_cf):
    capacity = raw_cf.getint('baseconf', 'server_capacity')
    type_num = raw_cf.getint('baseconf', 'num_vm_types')
    dimension_num = raw_cf.getint('baseconf', 'num_of_dimensions')

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        current_num = raw_cf.getint('num_of_block', str(i))
        type_ave_num_list.append(int(current_num))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        current_demands = raw_cf.get('VM_demands', str(i))
        demands_str = current_demands.split(',')
        for j in range(0, int(dimension_num)):
            item_type.append(int(demands_str[j]))
        item_type_list.append(item_type)

    total_server_num = 0
    pattern_types = pattern_cf.options('pattern_types')
    for i in range(len(pattern_types)):
        pattern_num = pattern_cf.getint('pattern_num', str(i))
        total_server_num = total_server_num + pattern_num

    # initialize vm_warehouse
    vm_warehouse = []
    for i in range(type_num):
        for j in range(type_ave_num_list[i]):
            vm_warehouse.append(i)

    # initialize the server_warehouse
    server_warehouse = []
    for i in range(total_server_num):
        initial_pattern = []
        for j in range(type_num):
            initial_pattern.append(0)
        server_warehouse.append(initial_pattern)

    # performing bestfit max min pattern generation
    while len(vm_warehouse) > 0:
        current_index = random.randint(0, len(vm_warehouse) - 1)
        current_vm_type = vm_warehouse[current_index]
        current_vm__demands = item_type_list[current_vm_type]
        DVol_index = 0
        DVol_val = sys.maxint
        find_flag = False
        for i in range(total_server_num):
            used_resource_list = cal_current_used_resource(type_num, dimension_num, item_type_list, server_warehouse[i])
            checking_flag = firstfit_checking(item_type_list[current_vm_type], used_resource_list, capacity)
            if checking_flag:
                current_DVol_val = cal_Max_DVol_value(capacity, used_resource_list)
                if current_DVol_val < DVol_val:
                    DVol_val = current_DVol_val
                    DVol_index = i
                    find_flag = True

        if find_flag:
            server_warehouse[DVol_index][current_vm_type] = server_warehouse[DVol_index][current_vm_type] + 1

        del vm_warehouse[current_index]

    pattern_num_list = []
    for i in range(len(server_warehouse)):
        pattern_num_list.append(1)

    return server_warehouse, pattern_num_list


if __name__ == '__main__':
    # random.seed(RANDOM_SEED)
    raw_cf = ConfigParser.ConfigParser()
    raw_cf.read('/Users/jiyuanshi/My_Data/PYTHON/CSCWD/conf_files/random/raw_0.ini')
    pattern_cf = ConfigParser.ConfigParser()
    pattern_cf.read('/Users/jiyuanshi/My_Data/PYTHON/CSCWD/conf_files/random/pattern_raw_0.ini')

    pattern_list, pattern_num_list = firstfit_generation(raw_cf, pattern_cf)
    print('pattern_list: %s' % (pattern_list))
    print('len(pattern_list): %s' % (len(pattern_num_list)))
    total_vms = []
    for i in range(len(pattern_list[0])):
        total_vms.append(0)
    for i in range(len(pattern_list)):
        for j in range(len(total_vms)):
            total_vms[j] = total_vms[j] + pattern_list[i][j]
    print('total_vms: %s' % (total_vms))

    pattern_list, pattern_num_list = max_min_generation(raw_cf, pattern_cf)
    print('pattern_list: %s' % (pattern_list))
    print('len(pattern_list): %s' % (len(pattern_num_list)))
    total_vms = []
    for i in range(len(pattern_list[0])):
        total_vms.append(0)
    for i in range(len(pattern_list)):
        for j in range(len(total_vms)):
            total_vms[j] = total_vms[j] + pattern_list[i][j]
    print('total_vms: %s' % (total_vms))

    pattern_list, pattern_num_list = vectordot_generation(raw_cf, pattern_cf)
    print('pattern_list: %s' % (pattern_list))
    print('len(pattern_list): %s' % (len(pattern_num_list)))
    total_vms = []
    for i in range(len(pattern_list[0])):
        total_vms.append(0)
    for i in range(len(pattern_list)):
        for j in range(len(total_vms)):
            total_vms[j] = total_vms[j] + pattern_list[i][j]
    print('total_vms: %s' % (total_vms))

    pattern_list, pattern_num_list = cos_generation(raw_cf, pattern_cf)
    print('pattern_list: %s' % (pattern_list))
    print('len(pattern_list): %s' % (len(pattern_num_list)))
    total_vms = []
    for i in range(len(pattern_list[0])):
        total_vms.append(0)
    for i in range(len(pattern_list)):
        for j in range(len(total_vms)):
            total_vms[j] = total_vms[j] + pattern_list[i][j]
    print('total_vms: %s' % (total_vms))

    pattern_list, pattern_num_list = Max_DVol_generation(raw_cf, pattern_cf)
    print('pattern_list: %s' % (pattern_list))
    print('len(pattern_list): %s' % (len(pattern_num_list)))
    total_vms = []
    for i in range(len(pattern_list[0])):
        total_vms.append(0)
    for i in range(len(pattern_list)):
        for j in range(len(total_vms)):
            total_vms[j] = total_vms[j] + pattern_list[i][j]
    print('total_vms: %s' % (total_vms))
