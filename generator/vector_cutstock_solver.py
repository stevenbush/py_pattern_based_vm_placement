"""
cutstock.py:  use gurobi for solving an variant of cutting stock problem, we call this problem vector cutting stock problem.

The instance of the cutting stock problem is represented by the two
lists of m items of size and quantity s=(s_i) and q=(q_i).

The roll size is B.

Given packing patterns t_1, ...,t_k,...t_K where t_k is a vector of
the numbers of items cut from a roll, the problem is reduced to the
following LP:
    
    minimize   sum_{k} x_k
    subject to sum_{k} t_k(i) x_k >= q_i    for all i
	       x_k >=0			    for all k.

We apply a column generation approch (Gilmore-Gomory approach) in
which we generate cutting patterns by solving a knapsack sub-problem.

Copyright (c) by Jiyuanshi at Southeast University, 2010
"""

import math, random, os, csv, time
import sys
from gurobipy import *

LOG = False
EPS = 1.e-6


def CuttingStockExample1():
    """CuttingStockExample1: create toy instance for the cutting stock problem."""
    B = 110  # roll width (bin size)
    w = [20, 45, 50, 55, 75]  # width (size) of orders (items)
    q = [48, 35, 24, 10, 8]  # quantitiy of orders
    s = []
    for j in range(len(w)):
        for i in range(q[j]):
            s.append(w[j])
    return s, B


def CuttingStockExample2():
    """CuttingStockExample2: create toy instance for the cutting stock problem."""
    B = 9  # roll width (bin size)
    w = [2, 3, 4, 5, 6, 7, 8]  # width (size) of orders (items)
    q = [4, 2, 6, 6, 2, 2, 2]  # quantitiy of orders
    s = []
    for j in range(len(w)):
        for i in range(q[j]):
            s.append(w[j])
    return s, B


def ReadingConfFile(conf_file_path):
    # print(conf_file_path)
    conf_file = file(conf_file_path, 'rb')
    reader = csv.reader(conf_file)
    rows = [row for row in reader]
    conf_file.close()

    capacity = rows[0][0]
    type_num = rows[1][0]
    dimension_num = rows[2][0]

    capacity_list = []
    for i in range(0, int(dimension_num)):
        capacity_list.append(int(capacity))

    type_ave_num_list = []
    for i in range(0, int(type_num)):
        type_ave_num_list.append(int(rows[3][i]))

    item_type_list = []
    for i in range(0, int(type_num)):
        item_type = []
        for j in range(0, int(dimension_num)):
            item_type.append(int(rows[i + 4][j]))
        item_type_list.append(item_type)

    return item_type_list, type_ave_num_list, capacity_list


def solveCuttingStock(s, q, B):
    """solveCuttingStock: use Haessler's heuristic.

    Parameters:
        s - list with item widths
        B - bin capacity

    Returns a solution: list of lists, each of which with the cuts of a roll.
    """
    w = s  # list of different widths (sizes) of items
    q = q  # quantitiy of orders

    t = []  # patterns
    m = len(w)
    # generate initial patterns with one size for each item width
    for i, width in enumerate(w):
        pat = [0] * m  # vector of number of orders to be packed into one roll (bin)
        temp_list = [0] * len(B)
        for j in range(len(B)):
            temp_list[j] = int(B[j] / width[j])
        pat[i] = min(temp_list)
        t.append(pat)

    if LOG:
        print "sizes of orders=", w
        print "quantities of orders=", q
        print "roll size=", B
        print "initial patterns", t

    iter = 0
    K = len(t)
    master = Model("LP")  # master LP problem
    x = {}
    for k in range(K):
        x[k] = master.addVar(obj=1, vtype="I", name="x[%d]" % k)
    master.update()

    orders = {}
    for i in range(m):
        coef = [t[k][i] for k in range(K) if t[k][i] > 0]
        var = [x[k] for k in range(K) if t[k][i] > 0]
        orders[i] = master.addConstr(LinExpr(coef, var), ">=", q[i], name="Order[%d]" % i)

    master.update()  # must update before calling relax()
    master.Params.OutputFlag = 0  # silent mode
    # master.write("MP" + str(iter) + ".lp")

    while 1:
        iter += 1
        relax = master.relax()
        relax.optimize()
        pi = [c.Pi for c in relax.getConstrs()]  # keep dual variables

        knapsack = Model("KP")  # knapsack sub-problem
        knapsack.ModelSense = -1  # maximize
        y = {}
        for i in range(m):
            y[i] = knapsack.addVar(obj=pi[i], ub=q[i], vtype="I", name="y[%d]" % i)
        knapsack.update()

        for d in range(len(B)):
            temp_w = [0] * m
            for n in range(m):
                temp_w[n] = w[n][d]
            L = LinExpr(temp_w, [y[i] for i in range(m)])
            knapsack.addConstr(L, "<=", B[d], name="width[%d]" % d)

        knapsack.update()
        # knapsack.write("KP"+str(iter)+".lp")
        knapsack.Params.OutputFlag = 0  # silent mode
        knapsack.optimize()
        if LOG:
            print "objective of knapsack problem:", knapsack.ObjVal
        if knapsack.ObjVal < 1 + EPS:  # break if no more columns
            break

        # pat = [int(y[i].X + 0.5) for i in y]  # new pattern
        pat = [int(math.ceil(y[i].X)) for i in y]  # new pattern
        t.append(pat)
        if LOG:
            print "shadow prices and new pattern:"
            for i, d in enumerate(pi):
                print "\t%5d%12g%7d" % (i, d, pat[i])
            print

        # add new column to the master problem
        col = Column()
        for i in range(m):
            if t[K][i] > 0:
                col.addTerms(t[K][i], orders[i])
        x[K] = master.addVar(obj=1, vtype="I", name="x[%d]" % K, column=col)
        master.update()  # must update before calling relax()
        # master.write("MP" + str(iter) + ".lp")
        K += 1

    for i in range(m):
        coef = [t[k][i] for k in range(K) if t[k][i] > 0]
        var = [x[k] for k in range(K) if t[k][i] > 0]
        orders[i] = master.addConstr(LinExpr(coef, var), ">=", q[i], name="Order[%d]" % i)

    master.update()  # must update before calling relax()

    # Finally, solve the IP
    if LOG:
        master.Params.OutputFlag = 1  # verbose mode
    master.optimize()

    patterns_num = 0

    patterns_list = {}

    if LOG:
        print
        print "final solution (integer master problem):  objective =", master.ObjVal
        print "patterns:"
        for k in x:
            if x[k].X > EPS:
                patterns_num = patterns_num + 1
                print "pattern", k,
                print "\tsizes:",
                print [w[i] for i in range(m) if t[k][i] > 0 for j in range(t[k][i])],
                # print "--> %d rolls" % int(x[k].X + .5)
                print "--> %d rolls" % int(math.ceil(x[k].X))

    for k in x:
        if x[k].X > EPS:
            patterns_list[str(t[k])] = int(math.ceil(x[k].X))

    rolls = []
    for k in x:
        # for j in range(int(x[k].X + .5)):
        for j in range(int(math.ceil(x[k].X))):
            rolls.append(sorted([w[i] for i in range(m) if t[k][i] > 0 for j in range(t[k][i])]))
    rolls.sort()
    return patterns_list, rolls


if __name__ == "__main__":

    if len(sys.argv) < 3:
        print('No ratio_standard and input_configuration_file specified!!!!')
        print('Please enter python script_name ratio_standard input_configuration_file')
        sys.exit()

    ratio_standard = float(sys.argv[1])
    input_file_name = str(sys.argv[2])
    conf_file_path = os.path.abspath(os.curdir) + '/' + str(sys.argv[2])
    if LOG:
        print('ratio_standard=' + str(sys.argv[1]))
        print conf_file_path

    s, q, B = ReadingConfFile(conf_file_path)

    if LOG:
        print('item_type_list: ' + str(s))
        print('item_num_list: ' + str(q))
        print('capacity_list: ' + str(B))

    print "\n\nCutting stock problem:-----------------------"
    start = time.clock()
    patterns_list, rolls = solveCuttingStock(s, q, B)
    end = time.clock()
    print len(rolls), "rolls:"
    print('paterns_num: ' + str(len(patterns_list)))
    print('patterns_list: ')
    sorted_list = sorted(patterns_list.items(), lambda x, y: cmp(x[1], y[1]), reverse=True)
    for item in sorted_list:
        print(str(item[0]) + ':' + str(item[1]))

    print('-------')

    dominating_ratio = 0
    out_pattern_list = {}
    result_item_type_num = [0] * len(s)
    ratio_list = []
    for pattern_info in sorted_list:
        if dominating_ratio <= ratio_standard:
            dominating_ratio = dominating_ratio + float(pattern_info[1]) / float(len(rolls))
            ratio_list.append(round(float(pattern_info[1]) / float(len(rolls)), 3))
            out_pattern_list[str(pattern_info[0])] = pattern_info[1]
            pattern_array = pattern_info[0].replace(', ', ',')[1: -1].split(',')
            for i in range(len(pattern_array)):
                result_item_type_num[i] = result_item_type_num[i] + int(pattern_array[i]) * int(pattern_info[1])
        else:
            break

    print('result_item_type_num: ' + str(result_item_type_num))
    print('original_item_type_num: ' + str(q))

    out_sorted_list = sorted(out_pattern_list.items(), lambda x, y: cmp(x[1], y[1]), reverse=True)

    output_matching_file_name = 'out_matching_' + input_file_name
    output_matching_file_path = os.path.abspath(os.curdir) + '/' + output_matching_file_name
    output_file = file(output_matching_file_path, 'wb')
    writer = csv.writer(output_file)

    print('output_matching_dict------------')
    counter = 0
    for item in out_sorted_list:
        print(item[0] + ':' + str(item[1]) + ':' + str(ratio_list[counter]))
        output_list = [str(item[0]), str(item[1])]
        writer.writerow(output_list)
        counter = counter + 1

    output_file.close()

    print('runtime: ' + str(end - start))
